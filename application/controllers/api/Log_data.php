<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_data extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_log');
	}

	public function add()
	{
		$data = $this->input->post();

		if ($this->M_log->add($data))
		{
			$result = array(
				'message' => 'Log data berhasil disimpan.'
			);

			$this->response(200, $result);
		}
		else
		{
			$result = array(
				'message' => 'Terjadi error, log data gagal disimpan.',
				'error'   => 'Insert db error.'
			);

			$this->response(500, $result);
		}
	}

	public function get_all()
	{
		$logs = $this->M_log->get_all();

		if ( !$logs )
		{
			$result = array(
				'message' => 'No data found.'
			);

			$this->response(404, $result);
		}

		$result = array(
			'logs' => $logs
		);

		$this->response(200, $result);

	}

}

/* End of file Log_data.php */
/* Location: ./application/controllers/api/Log_data.php */