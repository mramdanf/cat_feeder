<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Makanan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('custom_helper');
        $this->load->model('M_makanan');

        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        $content['makanans']   = $this->M_makanan->get_all();
        $content['page_view'] = 'makanan_list';
        $content['title']     = 'Makanan';

        $this->load->view('template', $content);
    }

    public function add()
    {
        $content['page_view'] = 'makanan_add';
        $content['title']     = 'Tambah Makanan';

        $this->load->view('template', $content);
    }

    public function do_add()
    {
        $data = $this->input->post();

        if ($this->M_makanan->add($data))
        {
            ch_set_flash('success', 'Data makanan berhasil disimpan.');
        }
        else
        {
            ch_set_flash('failed', 'Terjadi error, data makanan gagal disimpan.');
        }

        redirect('makanan');
    }

    public function delete()
    {
        $id = $this->input->post('id');

        if ($this->M_makanan->delete($id))
        {
            ch_set_flash('success', 'Data makanan berhasil dihapus.');
        }
        else
        {
            ch_set_flash('failed', 'Terjadi error, data makanan gagal dihapus.');
        }
    }

    public function edit($id)
    {
        $content['makanan'] = $this->M_makanan->get($id);

        $content['page_view'] = 'makanan_edit';
        $content['title']     = 'Edit Makanan';

        $this->load->view('template', $content);
    }

    public function do_edit()
    {
        $data = $this->input->post();

        if ($this->M_makanan->edit($data))
        {
            ch_set_flash('success', 'Data makanan berhasil diubah.');
        }
        else
        {
            ch_set_flash('failed', 'Terjadi error, data makanan gagal diubah.');
        }

        redirect('makanan');
    }

}

?>