
ALTER TABLE  `makanan` ADD  `status` ENUM(  'Active',  'Inactive' ) NULL ;

ALTER TABLE  `makanan` CHANGE  `status`  `status` ENUM(  'Active',  'Inactive' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Inactive';