<section class="content-header">
  <h1>
    Stok Makanan
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Stok Makanan</li>
  </ol>
</section>

<section class="content">
  <?=ch_falert()?>
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <a href="<?=site_url('makanan/add')?>" class="btn btn-primary">Add</a>
        </div>
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover" id="food-table">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Initial Stock</th>
                <th>Current Stock</th>
                <th>Tanggal Refill</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach ($makanans as $makanan): ?>
              <tr>
                <td><?=$no++?></td>
                <td data-name="nama"><?=$makanan['nama']?></td>
                <td><?=$makanan['initial_stock']?></td>
                <td><?=$makanan['current_stock']?></td>
                <td><?=date('d M Y - H:i:s', strtotime($makanan['tgl_refill']))?></td>
                <td><?=($makanan['status']=='Active')?'<span class="label label-success">Active</span>':'<span class="label label-danger">Inactive</span>'?></td>
                <td>
                  <a 
                    href="<?=site_url('makanan/edit/'.$makanan['id'])?>" 
                    title="Edit"><span class="fa fa-pencil"></span></a>&nbsp;&nbsp;&nbsp;
                  <a 
                    href="#" 
                    title="delete" 
                    id="delete"
                    data-id="<?=$makanan['id']?>"
                    style="color: red"><span class="fa fa-trash"></span></a>
                </td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="<?=base_url('assets/js')?>/makanan_list.js"></script>