<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_jadwal');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function get_all()
	{
		$jadwals = $this->M_jadwal->get_all();

		if (count($jadwals) > 0)
		{
			foreach ($jadwals as $key => $s) 
			{
				// Hapus data yg tidak perlu
				unset($jadwals[$key]['id']);
				unset($jadwals[$key]['jam']);
				unset($jadwals[$key]['nama']);

				// Ubah makanan_id ke int
				$jadwals[$key]['makanan_id'] = (int)$jadwals[$key]['makanan_id'];
			}

			$res = array(
				'today'   => date('G,'.intval(date('i')).','.intval(date('s')).',j,n,y'),
                'jadwals' => $jadwals
			);

			$this->response(200, $res);
		}
		else
		{
			$res = array(
                'message' => 'No data found'
			);

			$this->response(404, $res);
		}
	}

}

/* End of file Jadwal.php */
/* Location: ./application/controllers/api/Jadwal.php */