<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_log extends CI_Model {

	var $table = 'log';

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function add($data = array())
	{
		$data['created_at'] = date('Y-m-d H:i:s');

		$insert = $this->db->insert($this->table, $data);

        if ($insert)
        {
            // Kurangi stock makanan
            $this->load->model('M_makanan');
            $konsumsi = $this->M_makanan->substract_stock($data['makanan_id']);
        }

        return $insert;
	}

	public function get_all()
	{
		$logs = $this->db->get($this->table);

		return ($logs->num_rows() > 0) ? $logs->result_array() : FALSE;
	}

	public function konsumsi_makanan($start_date = "", $end_date = "", $makanan_id)
	{
		$start_date = date('Y-m-d', strtotime($start_date));
		$end_date   = date('Y-m-d', strtotime($end_date));

		// Grouping data berdasarkan tanggal, kemudian filter
		// data untuk range tanggal >= $start_date dan <= $end_date
		$dates = $this->db->select('id, created_at')
                          ->where('makanan_id', $makanan_id)
						  ->group_by('DATE(created_at)')
		                  ->having('DATE(created_at) >=', $start_date)
		                  ->having('DATE(created_at) <=', $end_date)
		                  ->get($this->table);
        
        if ($dates->num_rows() > 0)
        {
        	$dates = $dates->result_array();

        	foreach ($dates as $key => $date) 
        	{
        		// Cari data log makan untuk setiap tanggal
        		$logs = $this->db->where('DATE(created_at)', date('Y-m-d', strtotime($date['created_at'])))
        						 ->order_by('id', 'ASC')
        		                 ->get($this->table)
        		                 ->result_array();

        		$konsumsi = 0;
        		for ($i=0; $i < count($logs); $i++) 
        		{ 
        			// Kalkulasikan banyaknya konsumsi makanan dengan
        			// menjumlahkan selisih antara after_feed dengan 
        			// before_feed
        			if (($i + 1) < count($logs))
        			{
        				$selisih   = $logs[$i]['after_feed'] - $logs[$i+1]['before_feed'];
	        			$konsumsi += $selisih;	
        			}
        		}

        		$dates[$key]['created_at_text'] = date('d M Y', strtotime($date['created_at']));

        		$dates[$key]['konsumsi'] = $konsumsi;
        		$dates[$key]['logs']     = $logs;
        	}

        	return $dates;
        }
        else
        {
        	return FALSE;
        }

	}

}

/* End of file M_log.php */
/* Location: ./application/models/api/M_log.php */