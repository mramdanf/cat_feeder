<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jadwal extends CI_Model {

	var $table = 'schedule';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_all()
	{
		$schedules = $this->db->select('s.*, m.nama')
		                      ->from($this->table.' s')
							  ->join('makanan m', 's.makanan_id = m.id')
							  ->order_by('id', 'DESC')
							  ->get();
        
        if ($schedules->num_rows() > 0)
        {
        	$schedules = $schedules->result_array();

        	foreach ($schedules as $key => $s) 
			{
				$str_time = strtotime($s['jam']);
				$schedules[$key]['jam_txt'] = date('G', $str_time).','.intval(date('i', $str_time)).','.intval(date('s', $str_time));
			}

			return $schedules;
        }
        else
        {
        	return array();
        }
	}

	public function delete($id = NULL)
	{
		return $this->db->delete($this->table, array('id' => $id));
	}

	public function add($post = array())
	{
		return $this->db->insert($this->table, $post);
	}

	public function edit($post = array())
	{
		$id = $post['id'];
		unset($post['id']);

		return $this->db->update($this->table, $post, array('id' => $id));
	}

	public function get($id = NULL)
	{
		$schedule = $this->db->get_where($this->table, array('id' => $id));

		return ($schedule->num_rows() > 0) ? $schedule->row_array() : array(); 
	}

}

/* End of file M_jadwal.php */
/* Location: ./application/models/M_jadwal.php */