var baseUrl     = window.location.origin;
var uriSegments = window.location.pathname.split('/');
baseUrl        += '/' + uriSegments[1] + '/';

$(document).ready(function() {

	$('.datepicker').datepicker({
		format: 'dd M yyyy',
		autoclose: true
    })
	
	intervalLoadStatusAlat();

	getFoodStock();

	getStatusAlat();

	loadGrafik();

	$('#box-status-alat button#refresh').click(function(e) {		
		getStatusAlat();
	});

	$('#box-chart button#refresh').click(function(e) {
		loadGrafik();
	});

	$('#box-chart .form-inline button').click(function(event) {
		loadGrafik();
	});

	// Refresh stock
	$('div.bg-aqua').find('span.btn-refresh-top').click(function(event) {
		console.log('test');
		getFoodStock();
	});

});

function intervalLoadStatusAlat() 
{
	console.log('Interval status alat...');

	$.ajax({
		url: baseUrl + 'api/alat/get',
		type: 'GET',
		data: {
			id: 1
		},
	})
	.done(function(resp) {
		var interval = (parseInt(resp.alat.update_interval)+6)*1000;
		
		setInterval(getStatusAlat, interval);
		console.log('Interval set to: ' + interval);

		var i = 1;
		setInterval(
			function() {
				//console.log(i++);
				if (i==(interval/1000)) i=1;
			},
			1000
		);
	})
	.fail(function(err) {
		console.log("error");
		console.log(err);
	});
	

}

function getFoodStock()
{
	var $wrapper = $('div#stock-wrapper');
	var $content = $wrapper.find('.info-box-number');
	var $progressbar = $wrapper.find('.progress-bar');
	var $description = $wrapper.find('.progress-description');

	$wrapper.LoadingOverlay('show');
	$.ajax({
		url: baseUrl + 'dashboard/food_stock',
	})
	.done(function(resp) {

		$wrapper.LoadingOverlay('hide');

		var currStrock = resp.food.current_stock;
		var initStock  = resp.food.initial_stock;
		var persentase = Math.ceil((currStrock/initStock)*100) + '%';

		$content.text(resp.food.current_stock + ' / ' + resp.food.initial_stock);
		$progressbar.css('width', persentase);

		if (currStrock != 0) {
			$description.text(persentase + ' dari stok awal');
			$wrapper.removeClass('bg-red');
			$wrapper.addClass('bg-aqua');
		}
		else {
			$description.text('Stock makanan habis.');
			$wrapper.removeClass('bg-aqua');
			$wrapper.addClass('bg-red');
		}

	})
	.fail(function() {
		$wrapper.LoadingOverlay('hide');
		console.log("error");
	});	
	
}

function getStatusAlat()
{
	console.log('Get status alat...');

	var $boxStatuAlat = $('#box-status-alat');
	var $tableAlat    = $boxStatuAlat.find('table#alat-desc');

	$boxStatuAlat.LoadingOverlay('show');
	$.ajax({
		url: baseUrl + 'dashboard/status_alat'
	})
	.done(function(resp) {
		$boxStatuAlat.LoadingOverlay('hide');

		$tableAlat.find('td[data-name=nama]').text(resp.alat.nama);
		$tableAlat.find('td[data-name=update_interval]').text(resp.alat.update_interval + ' detik');
		$tableAlat.find('td[data-name=status]').text(resp.alat.status);
		$tableAlat.find('td[data-name=last_update]').text(resp.alat.updated_at);

		if (resp.alat.status == 'OFF')
			$boxStatuAlat.find('#lamp-img').attr('src', baseUrl + 'assets/img/pic_bulboff.gif');
		else if (resp.alat.status == 'ON')
			$boxStatuAlat.find('#lamp-img').attr('src', baseUrl + 'assets/img/pic_bulbon.gif');
	})
	.fail(function() {
		$boxStatuAlat.LoadingOverlay('hide');
		console.log("error");
	});
	
}

function loadGrafik(startDate, endDate)  {

	var $inStartDate = $('input[name=start_date]');
	var $inEndDate   = $('input[name=end_date]');

	var paramStartDate = $inStartDate.val();
	var paramEndDate   = $inEndDate.val();

	if (paramStartDate == '') {
		paramStartDate = moment().startOf('isoWeek').format('YYYY-MM-DD');
		$inStartDate.val(paramStartDate);
	}

	if (paramEndDate == '') {
		paramEndDate = moment().endOf('isoWeek').format('YYYY-MM-DD');
		$inEndDate.val(paramEndDate);
	}

	var $boxCart = $('div#box-chart');
	var $chartContainer = $boxCart.find('#container');
	var $chartMessage   = $boxCart.find('p#chart-msg');
	var $chartWrapper   = $boxCart.find('div.chart');

	$chartMessage.hide();

	console.log('Load grafik makan...');
	$boxCart.LoadingOverlay('show');
	$.ajax({
		url: baseUrl + 'dashboard/konsumsi_makanan',
		data: {
			start_date: paramStartDate,
			end_date: paramEndDate,
			makanan_id: $('select[name=makanan_id]').val()
		}
	})
	.done(function(resp) {
		$boxCart.LoadingOverlay('hide');

		if (resp.status.code == 404) {
			$chartContainer.hide();

			$chartWrapper.addClass('hidden-chart');

			$chartMessage.text('No data found');
			$chartMessage.show();
		
		} else if (resp.status.code == 200) {
			$chartContainer.show();
			$chartMessage.hide();

			$chartWrapper.removeClass('hidden-chart');

			var dtCategories = resp.konsumsi.map(function(elem){
				return elem.created_at_text;
			});

			var dtDimakanKucing = resp.konsumsi.map(function(elem){
				return elem.konsumsi;
			});

			var myChart = Highcharts.chart('container', {
				chart: {
					type: 'line'
				},
				title: {
					text: 'Konsumsi Makanan'
				},
				xAxis: {
					categories: dtCategories
				},
				yAxis: {
					title: {
						text: 'Banyaknya Makanan'
					}
				},
				series: [
					{
						name: 'Dimakan Kucing',
						data: dtDimakanKucing
					}
				]
			});

		}

			
	})
	.fail(function() {
		$boxCart.LoadingOverlay('hide');
		console.log("error");
	});
	
}