<section class="content-header">
  <h1>
    Tambah Alat
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Tambah Alat</li>
  </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<form 
					action="<?=site_url('alat/do_add')?>"
					method="POST">
					<div class="box-body">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="nama" class="form-control">
							</div>
							<div class="form-group">
								<label>Update Interval</label>
								<input type="number" name="update_interval" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Status</label>
								<select 
									name="status" 
									class="form-control">
									<option value=""></option>
									<option value="ON">ON</option>
									<option value="OFF">OFF</option>
								</select>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="pull-right">
							<a href="javascript: window.history.back();" class="btn btn-danger">Back</a>
							<button type="submit" class="btn btn-success">Submit</button>	
						</div>
						
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script src="<?=base_url('assets/js')?>/setting_jadwal_add.js" type="text/javascript"></script>