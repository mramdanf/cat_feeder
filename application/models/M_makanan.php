<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_makanan extends CI_model {

    var $table = 'makanan';

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper('custom_helper');
    }

    public function get_all()
    {
        $this->db->order_by('status', 'ASC');
        $makanans = $this->db->get($this->table);

        return ($makanans->num_rows() > 0) ? $makanans->result_array() : array();
    }

    public function add($data = array())
    {
        $data['current_stock'] = $data['initial_stock'];
        $data['tgl_refill']    = date('Y-m-d H:i:s');

        return $this->db->insert($this->table, $data);
    }

    public function delete($id = NULL)
    {
        return $this->db->delete($this->table, array('id' => $id));
    }

    public function get($id = NULL)
    {
        return $this->db->get_where($this->table, array('id' => $id))
                        ->row_array();
    }

    public function edit($data = array())
    {
        $id = $data['id']; unset($data['id']);

        $data['current_stock'] = $data['initial_stock'];
        $data['tgl_refill']    = date('Y-m-d H:i:s');

        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function substract_stock($id = '')
    {
        $konsumsi = $this->get_konsumsi_makanan();

        $sql = "UPDATE makanan SET current_stock = (initial_stock - $konsumsi) WHERE id = $id";
        return $this->db->query($sql);
    }

    public function get_active_food()
    {
        $food = $this->db->get_where($this->table, array('status' => 'Active'));

        return ($food->num_rows() > 0) ? $food->row_array() : FALSE;
    }

    public function get_konsumsi_makanan()
    {
        // Cari data log makan untuk setiap tanggal
        $logs = $this->db->order_by('id', 'ASC')
                         ->get('log')
                         ->result_array();

        $konsumsi = 0;
        for ($i=0; $i < count($logs); $i++) 
        { 
            // Kalkulasikan banyaknya konsumsi makanan dengan
            // menjumlahkan selisih antara after_feed dengan 
            // before_feed
            if (($i + 1) < count($logs))
            {
                $selisih   = $logs[$i]['after_feed'] - $logs[$i+1]['before_feed'];
                $konsumsi += $selisih;  
            }
        }

        return $konsumsi;
    }
}

?>