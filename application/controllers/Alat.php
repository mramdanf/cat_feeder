<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_alat');
		$this->load->helper('custom_helper');
	}

	public function index()
	{
		$content['alats']     = $this->M_alat->get_all();
		$content['page_view'] = 'alat_list';
		$content['title']     = 'Pengaturan Alat';

		$this->load->view('template', $content);
	}

	public function add()
	{
		$content['page_view'] = 'alat_add';
		$content['title']     = 'Tambah Alat';

		$this->load->view('template', $content);
	}

	public function do_add()
	{
		$data = $this->input->post();

		if ($this->M_alat->add($data))
		{
			ch_set_flash('success', 'Alat berhasil ditambahkan.');
		}
		else
		{
			ch_set_flash('failed', 'Terjadi error, gagal menyimpan data alat.');	
		}

		redirect('alat');
	}

	public function edit($id = NULL)
	{
		$content['alat']      = $this->M_alat->get($id);
		$content['page_view'] = 'alat_edit';
		$content['title']     = 'Update Alat';

		$this->load->view('template', $content);
	}

	public function do_edit()
	{
		$data = $this->input->post();

		if ($this->M_alat->edit($data))
		{
			ch_set_flash('success', 'Alat berhasil diubah.');
		}
		else
		{
			ch_set_flash('failed', 'Terjadi error, gagal mengubah data alat.');	
		}

		redirect('alat');
	}

	public function delete()
	{
		$id = $this->input->post('id');

		if ($this->M_alat->delete($id))
		{
			ch_set_flash('success', 'Alat berhasil dihapus.');
		}
		else
		{
			ch_set_flash('failed', 'Terjadi error, gagal menghapus data alat.');	
		}
	}

}

/* End of file Alat.php */
/* Location: ./application/controllers/Alat.php */